# 1.0.0 21.05.2024
* Neue Funktion: Sofort Stornierung der letzten Buchung. 
* Verbesserung: Mehr Details zum letzten Verkauf.

# 0.0.5 13.02.2024
Erstinstallation.