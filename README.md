Mit dieser Kasse können Buchungen vorgenommen werden, damit zwischen den verschiedenen Mehrwertsteuersätzen unterschieden werden kann.

# Bedienung
## Tasten
Die Zahlen sind selbsterklärend. **B** bedeutet Buch und somit der tiefe Mehrwertsteuersatz. **B-** die Stornierung davon. **S**, respektive **S-** bedeutet dagegen der geringe Mehrwertsteuersatz.

Mit **C** wird die letzte Zahl gelöscht, mit **CE** wird jedoch die ganze Rechnung zurückgesetzt.

Die dunkelgrünen Tasten senden übermitteln die Daten an die Webseite.

Auch eine Bedienung über den Zahlenblock ist möglich. Dies muss zuvor ⌨-Button eingeschaltet werden. Zum Buch gelten folgende Regeln.
* Enter → C
* Shift + Enter → C-
* Ctrl + Enter → S
* Shift + Ctrl + Enter → S- 

Die Notizen können als einfach Kassenbon oder auch als Erinnerung per Mail versandt werden, wenn das Gerät über ein gültiges E-Mailkonto verfügt. Dazu die ✉-Taste verwenden.

## Ablauf
Zuerst muss ein Büchertisch angelegt werden. Dazu muss man sich im Backend der Seite einloggen und zur entsprechenden Komponente gehen. Danach kann man unter dem Menüpunkt "Neuer Anlass" einen neuen Büchertisch anlegen. Ein Büchertisch ist immer auf einen Tag beschränkt. Öffnet man an diesem Tag die Kasse, kann der Büchertisch ausgewählt werden. Damit soll verhindert werden, dass man am falschen Büchertisch abrechnet. Es können pro Tag mehrere Büchertische erfasst werden.

Dann wird die Kasse bedient. Zuerst wird immer der Betrag eingegeben, danach der Warenposten. Ist ein Kauf abgeschlossen, wird je nach Zahlungsart die entsprechende Buchungstaste verwendet.

Nun erscheinen oben zwei Tasten, mit denen entweder die Zahlungsart der letzten Buchung geändert werden kann oder diese ganz Sorniert werden kann.

Am Ende des Tages kann im Backend die Zahlen ausgelesen werden.