const res = document.getElementById("result");
const book = document.getElementById("books");
const other = document.getElementById("other");
const total = document.getElementById("total");
const note = document.getElementById("note");
const event = document.getElementById("event_name");
const namebook = document.getElementById("namebook");
const nameother = document.getElementById("nameother");
const currency = document.getElementById("currency");
const vendoraddress = document.getElementById("vendoraddress");
const vendoremail = document.getElementById("vendoremail");

// Damit das Formular nicht zweimal nacheinander abgesandt wird.
if ( window.history.replaceState ) {
    window.history.replaceState( null, null, window.location.href );
}

// Prevent parsed as octal
// @link https://stackoverflow.com/questions/6718102/in-javascript-eval010-returns-8
function removeleadingnull(value) {
    return value.replace(/^0+/,'');
}

function addbook(value) {

    value = removeleadingnull(value);

    // Nur wenn das Dokument geladen ist.
    if (document.readyState === 'complete') {
        const calculatedValue = eval(value || null);
        if (isNaN(calculatedValue)) {
            res.value = "Ungültige Eingabe!";
            setTimeout(() => {
                res.value = "";
            }, 1300);
        } else {
            // Damit keine ungenauen Beträge entstehen.
            let number = (Math.ceil(calculatedValue * 20) / 20).toFixed(2);
            book.value = eval(book.value + '+' + number);
            res.value = "";

            getTotal(number);

            if (number > 0) {
                note.value += '+ ' + currency.innerText + ' ' + number + ' (' + namebook.value + ')\n';
            }
        }
    }
}
function cancelbook(value) {

    value = removeleadingnull(value);

    // Nur wenn das Dokument geladen ist.
    if (document.readyState === 'complete') {
        const calculatedValue = eval(value || null);
        if (isNaN(calculatedValue)) {
            res.value = "Ungültige Eingabe!";
            setTimeout(() => {
                res.value = "";
            }, 1300);
        } else {
            // Damit keine ungenauen Beträge entstehen.
            let number = (Math.ceil(calculatedValue * 20) / 20).toFixed(2);
            book.value = eval(book.value + '-' + number);
            res.value = "";

            minusTotal(number);
            if (number > 0) {
                note.value += '- ' + currency.innerText + ' ' + number + ' (' + namebook.value + ')\n';
            }
        }
    }
}

function addother(value) {

    value = removeleadingnull(value);

    // Nur wenn das Dokument geladen ist.
    if (document.readyState === 'complete') {
        const calculatedValue = eval(value || null);
        if (isNaN(calculatedValue)) {
            res.value = "Ungültige Eingabe!";
            setTimeout(() => {
                res.value = "";
            }, 1300);
        } else {
            // Damit keine ungenauen Beträge entstehen.
            let number = (Math.ceil(calculatedValue * 20) / 20).toFixed(2);
            other.value = eval(other.value + '+' + number);
            res.value = "";

            getTotal(number);
            if (number > 0) {
                note.value += '+ ' + currency.innerText + ' ' + number + ' (' + nameother.value + ')\n';
            }
        }
    }
}

function cancelother(value) {

    value = removeleadingnull(value);

    // Nur wenn das Dokument geladen ist.
    if (document.readyState === 'complete') {
        const calculatedValue = eval(value || null);
        if (isNaN(calculatedValue)) {
            res.value = "Ungültige Eingabe!";
            setTimeout(() => {
                res.value = "";
            }, 1300);
        } else {
            // Damit keine ungenauen Beträge entstehen.
            let number = (Math.ceil(calculatedValue * 20) / 20).toFixed(2);
            other.value = eval(other.value + '-' + number);
            res.value = "";

            minusTotal(number);
            if (number > 0) {
                note.value += '- ' + currency.innerText + ' ' + number + ' (' + nameother.value + ')\n';
            }
        }
    }
}

function getTotal(value) {
    total.value = eval(total.value + '+' + value).toFixed(2);
}

function minusTotal(value) {
    total.value = eval(total.value + '-' + value).toFixed(2);
}

function sendmail() {
    if(total.value) {
        var date = new Date();

        window.open(
                'mailto:?cc='+vendoremail.innerText+
                    '&body='+vendoraddress.innerHTML.replaceAll(/<(br|p)>/g,'%0D%0A').replaceAll(/<\/?[^>]+(>|$)/g, "")+
                    '%0D%0A Nr.: '+ date.getTime() +
                    '%0D%0A%0D%0A'+note.value.replaceAll('\n','%0D%0A')+
                    '%0D%0A'+total.placeholder+': '+currency.innerText+' '+total.value+
                    '%0D%0A%0D%0A'+date.toLocaleString()+'&subject='+event.innerText
                    );
    }
}

function deleteresult() {
    res.value = '';
}

function deleteall() {
    res.value = '';
    book.value = '';
    other.value = '';
    total.value = '';
    note.value = '';
}
function deleteonenumber() {
    const Input = res.value;
    //remove the last element in the string
    res.value = Input.substring(0, res.value.length - 1);
}

// Displays entered value on screen.
function liveScreen(enteredValue) {
    if (!res.value) {
        res.value = "";
    }
    res.value += enteredValue;
}
function disablekeyboardsupport() {
    document.removeEventListener("keydown", keyboardInputHandler);
    // document.getElementById("disablekeybordsupport").style.color = "#fff";
    document.getElementById("disablekeybordsupport").style.background = "darkgreen";

    // document.getElementById("addkeybordsupport").style.color = "#fff";
    document.getElementById("addkeybordsupport").style.background = "rgb(47, 51, 50)";
}
function addkeyboardsupport() {
    //adding event handler on the document to handle keyboard inputs
    document.addEventListener("keydown", keyboardInputHandler);

    document.getElementById("addkeybordsupport").style.background = "darkgreen";
    // document.getElementById("disablekeybordsupport").style.color = "#fff";
    document.getElementById("disablekeybordsupport").style.background = "rgb(47, 51, 50)";
}

//function to handle keyboard inputs
function keyboardInputHandler(e) {
    // to fix the default behavior of browser,
    // enter and backspace were causing undesired behavior when some key was already in focus.
    e.preventDefault();
    //grabbing the liveScreen

    //numbers
    if (e.key === "0") {
        res.value += "0";
    } else if (e.key === "1") {
        res.value += "1";
    } else if (e.key === "2") {
        res.value += "2";
    } else if (e.key === "3") {
        res.value += "3";
    } else if (e.key === "4") {
        res.value += "4";
    } else if (e.key === "5") {
        res.value += "5";
    } else if (e.key === "6") {
        res.value += "6";
    } else if (e.key === "7") {
        res.value += "7";
    } else if (e.key === "7") {
        res.value += "7";
    } else if (e.key === "8") {
        res.value += "8";
    } else if (e.key === "9") {
        res.value += "9";
    }

    //operators
    if (e.key === "+") {
        res.value += "+";
    } else if (e.key === "-") {
        res.value += "-";
    } else if (e.key === "*") {
        res.value += "*";
    } else if (e.key === "/") {
        res.value += "/";
    }

    //decimal key
    if (e.key === ".") {
        res.value += ".";
    }

    //press enter to see result
    if (e.shiftKey && e.ctrlKey && e.keyCode === 13) {
        cancelother(res.value);
    } else if (e.shiftKey && e.keyCode === 13) {
        cancelbook(res.value);
    } else if (e.ctrlKey && e.keyCode === 13) {
        addother(res.value);
    } else if (e.key === "Enter") {
        addbook(res.value);
    }

    //backspace for removing the last input
    if (e.key === "Backspace") {
        deleteonenumber();
    }
}