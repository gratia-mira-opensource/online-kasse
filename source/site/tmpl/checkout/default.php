<?php

/**
 * @package     Joomla.Administrator
 * @subpackage  com_onlinekasse
 *
 * @copyright   Copyright (C) 2023 Christoph Berger. All rights reserved.
 * @license     GNU General Public License version 3; see LICENSE
 * @since       0.5.0
 * @version     1.0.0
 */

use Joomla\CMS\Factory;
use Joomla\CMS\Language\Text;

// No direct access to this file
defined('_JEXEC') or die('Restricted Access');

$disabled = isset($_SESSION['Event']) ? '' : 'disabled="disabled"';

if (isset($this->Event->EventList))
{
	echo $this->Event->EventList;
}

if (!isset($_GET['gmd_komponentenansicht']))
{
	echo '<link rel="stylesheet" href="' . $this->baseurl . '/components/com_onlinekasse/tmpl/checkout/checkout.css">';
}
?>
    <div class="ok_message">
        <?php echo $this->Event->Message; ?>
    </div>
    <div id="action">
        <?php echo $this->Event->QucikAction; ?>
    </div>
    <form method="post">
        <div class="ok_wrapper">
            <h1 id="event_name"><?php echo $this->Event->EventName . ' (<span id="currency">' . $this->Event->EventCurrency . '</span>)'; ?></h1>
            <div class="ok_container">
                <div class="first-row">
                    <input type="text" name="result" id="result"
                           placeholder="<?php echo Text::_('COM_ONLINEKASSE_AMOUNT'); ?>" readonly/>
                    <input type="button" value="C" onclick="deleteresult()" id="clear-button-c"/>
                    <input type="button" value="CE" onclick="deleteall()" id="clear-button"/>
                </div>
                <div class="second-row">
                    <input type="button" value="1" onclick="liveScreen(1)"/>
                    <input type="button" value="2" onclick="liveScreen(2)"/>
                    <input type="button" value="3" onclick="liveScreen(3)"/>
                    <input type="button" id="namebook" value="<?php echo Text::_('COM_ONLINEKASSE_BOOKS_SHORT'); ?>"
                           onclick="addbook(result.value)" class="addproduct"/>
                </div>
                <div class="third-row">
                    <input type="button" value="4" onclick=" liveScreen(4)"/>
                    <input type="button" value="5" onclick=" liveScreen(5)"/>
                    <input type="button" value="6" onclick=" liveScreen(6)"/>
                    <input type="button" id="nameother" value="<?php echo Text::_('COM_ONLINEKASSE_OTHER_SHORT'); ?>"
                           onclick="addother(result.value)" class="addproduct"/>
                </div>
                <div class="fourth-row">
                    <input type="button" value="7" onclick="liveScreen(7)"/>
                    <input type="button" value="8" onclick="liveScreen(8)"/>
                    <input type="button" value="9" onclick=" liveScreen(9)"/>
                    <input type="button" value="<?php echo Text::_('COM_ONLINEKASSE_BOOKS_SHORT'); ?>-"
                           onclick="cancelbook(result.value)" class="removeproduct"/>
                </div>
                <div class="fifth-row">
                    <input type="button" value="*" onclick="liveScreen('*')"/>
                    <input type="button" value="0" onclick="liveScreen(0)"/>
                    <input type="button" value="." onclick="liveScreen('.')"/>
                    <input type="button" value="<?php echo Text::_('COM_ONLINEKASSE_OTHER_SHORT'); ?>-"
                           onclick="cancelother(result.value)" class="removeproduct"/>
                </div>
                <div class="sixth-row">
                    <input type="text" name="books" id="books"
                           placeholder="<?php echo Text::_('COM_ONLINEKASSE_BOOKS'); ?>" readonly/>
                    <input type="text" name="other" id="other"
                           placeholder="<?php echo Text::_('COM_ONLINEKASSE_OTHER'); ?>" readonly/>
                </div>
                <div class="seventh-row">
                    <input type="submit" name="card"
                           value="<?php echo Text::_('COM_ONLINEKASSE_CARD'); ?>" <?php echo $disabled ?> />
                    <input type="text" name="total" id="total"
                           placeholder="<?php echo Text::_('COM_ONLINEKASSE_TOTAL'); ?>" readonly/>
                    <input type="submit" name="cash"
                           value="<?php echo Text::_('COM_ONLINEKASSE_CASH'); ?>" <?php echo $disabled ?> />
                </div>
                <div class="eighth-row">
                    <input type="button" id="disablekeybordsupport" value="✎" onclick="disablekeyboardsupport()"
                           title="<?php echo Text::_('COM_ONLINEKASSE_CHECKOUT_NO_KEYBOARD_SUPPORT_TITLE'); ?>"/>
                    <input type="button" id="addkeybordsupport" value="⌨" onclick="addkeyboardsupport()"
                           title="<?php echo Text::_('COM_ONLINEKASSE_CHECKOUT_KEYBOARD_SUPPORT_TITLE'); ?>"/>
                    <input type="button" value=" " onclick="" title="empty" id="empty"/>
                    <input type="button" value="✉" onclick="sendmail()"
                           title="<?php echo Text::_('COM_ONLINEKASSE_CHECKOUT_SEND_MAIL'); ?>"/>
                </div>
                <div class="ninth-row">
                    <textarea name="note" id="note" rows="15" cols="35"></textarea>
                </div>
            </div>
            <div>
				<?php
				if (isset($_SESSION['Event']))
				{ ?>
                    <div class="event_info">
                        <p>
							<?php echo '<b>' . $this->Event->EventName . ' (' . $this->Event->EventLocation . ')</b><br>' . $this->Event->EventNotes; ?>
                        </p>
                    </div>
				<?php } ?>
                <div class="vendor_info">
                    <div id="vendoraddress">
						<?php echo $this->Event->Vendoradress; ?>
                    </div>
                    <div id="vendoremail">
						<?php echo $this->Event->Vendoremail; ?>
                    </div>
                </div>
            </div>
        </div>
    </form>

<?php
if (!isset($_GET['gmd_komponentenansicht']))
{
	echo '<script src="' . $this->baseurl . '/components/com_onlinekasse/tmpl/checkout/checkout.js"></script>';
}

if (isset($_SESSION['Eventlist']))
{
	echo '<div>' . $_SESSION['Eventlist'] . '</div>';
}