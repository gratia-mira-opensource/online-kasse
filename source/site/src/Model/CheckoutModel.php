<?php

namespace ChristophBerger\Component\OnlineKasse\Site\Model;

defined('_JEXEC') or die;

use Exception;
use Joomla\CMS\Factory;
use Joomla\CMS\Language\Text;
use Joomla\CMS\MVC\Model\ItemModel;

/**
 * @package     Joomla.Site
 * @subpackage  com_onlinekasse
 *
 * @copyright   Copyright (C) 2023 Christoph Berger. All rights reserved.
 * @license     GNU General Public License version 3; see LICENSE
 */

/**
 * Online Kasse Save Database Model
 * @since 0.0.5
 */
class CheckoutModel extends ItemModel
{

	/**
	 * Returns a message for display
	 *
	 * @param   integer  $pk  Primary key of the "message item", currently unused
	 *
	 * @return object Message object
	 * @throws Exception
	 * @since   0.0.5
	 * @version 1.0.0
	 */
	public function getItem($pk = null): object
	{


		// Initialise values
		$item = new \stdClass();


		$app                = Factory::getApplication();
		$params             = $app->getParams();
		$item->Vendoradress = $params->get('vendoradress');
		$item->Vendoremail  = $params->get('vendoremail');

		$get          = Factory::getApplication();
		$IncomingData = $get->input;

		// Lokale Informationen
		$current_event = $IncomingData->get('current_event', '', 'INT');

		// Objekt-Eigenschaften
		$item->Books      = $IncomingData->get('books', '', 'FLOAT');
		$item->Other      = $IncomingData->get('other', '', 'FLOAT');
		$item->Total      = $IncomingData->get('total', '', 'FLOAT');
		$item->Note       = $IncomingData->get('note', '', 'STRING');
		$item->Card       = $IncomingData->get('card', '', 'STRING');
		$item->Cash       = $IncomingData->get('cash', '', 'STRING');
		$item->Action     = $IncomingData->get('action', '', 'STRING');
		$item->Payment    = $IncomingData->get('payment', '', 'STRING');
		$item->PriceCheck = $IncomingData->get('price_check', '', 'STRING');

		// Event aus der Sessions übernehmen.
		if (isset($_SESSION['Event']) and !$current_event)
		{
			$item->EventName     = $_SESSION['Event']->name;
			$item->EventId       = $_SESSION['Event']->id;
			$item->EventCurrency = $_SESSION['Event']->currency;
			$item->EventLocation = $_SESSION['Event']->location;
			$item->EventNotes    = $_SESSION['Event']->notes;
		}
		// Event ermitteln.
		else
		{
			$db = Factory::getContainer()->get('DatabaseDriver');

			$query = $db->getQuery(true);
			$query->select('*');
			$query->from('#__ok_events');
			if ($current_event)
			{
				$query->where('id = ' . $db->quote($current_event));
			}
			else
			{
				$query->where('date = ' . $db->quote(date('Y-m-d')));
			}
			$db->setQuery($query);

			$items = $db->loadObjectList();

			if (!$items)
			{
				$item->Message       = Text::_('COM_ONLINEKASSE_MSG_NO_EVENT');
				$item->EventName     = 'Demo mode';
				$item->EventCurrency = 'CHF';
			}
			// Wenn nur 1 Event verfügbar, dieses nehmen.
			elseif (count($items) == 1)
			{
				// Item setzen
				$item                = $items[0];
				$item->EventName     = $items[0]->name;
				$item->EventId       = $items[0]->id;
				$item->EventCurrency = $items[0]->currency;
				$item->EventLocation = $items[0]->location;
				$item->EventNotes    = $items[0]->notes;

				$_SESSION['Event'] = $items[0];
			}
			// Wenn mehrere Anlässe vorhanden sind.
			else
			{
				$preform = '<div class="choose_event">
							<form method="post">
			                    <label for="option">' . Text::_('COM_ONLINEKASSE_SELECT_EVENT_LABEL') . '</label>
			                    <select id="current_event" name="current_event">';

				$list = '';
				foreach ($items as $SingleItem)
				{

					$list .= '<option value="' . $SingleItem->id . '">' . $SingleItem->name . '  (' . $SingleItem->currency . ')</option>';
				}

				$postform = '</select>
			                    <br><br>
			                    <input type="submit" value="' . Text::_('COM_ONLINEKASSE_SELECT_EVENT_SUBMIT') . '">
			                </form>
			                </div>';

				$item->EventList       = $preform . $list . $postform;
				$item->EventName       = 'Demo mode';
				$item->EventCurrency   = 'CHF';
				$_SESSION['Eventlist'] = $item->EventList;
			}
		}

		return $item;
	}

}