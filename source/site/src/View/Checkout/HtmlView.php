<?php

namespace ChristophBerger\Component\OnlineKasse\Site\View\Checkout;

defined('_JEXEC') or die;

require_once dirname(__FILE__, 3) . '/Helper/SaveSales.php';
require_once dirname(__FILE__, 3) . '/Helper/ManageLastSale.php';

use ChristophBerger\Component\OnlineKasse\Site\Helper\SaveSales;
use ChristophBerger\Component\OnlineKasse\Site\Helper\ManageLastSale;
use Exception;
use Joomla\CMS\MVC\View\HtmlView as BaseHtmlView;

/**
 * @package     Joomla.Site
 * @subpackage  com_onlinekasse
 *
 * @copyright   Copyright (C) 2023 Christoph Berger. All rights reserved.
 * @license     GNU General Public License version 3; see LICENSE
 */

/**
 * View for the user identity validation form
 * @since   0.5.0
 * @version 1.0.0
 */
class HtmlView extends BaseHtmlView
{


	/**
	 * Display the view
	 *
	 * @param   string  $template  The name of the layout file to parse.
	 *
	 * @return  void
	 * @throws Exception
	 * @version 1.0.0
	 * @since   0.0.2
	 */
	public function display($template = null): void
	{

		// Load some String
		if (isset($_GET['gmd_komponentenansicht']))
		{
			define('GMD_LIGHT_SEITENTITEL', 'Online-Kasse');
			define('GMD_LIGHT_HEAD', '<link rel="stylesheet" href="' . $this->baseurl . '/components/com_onlinekasse/tmpl/checkout/checkout.css">');
			define('GMD_LIGHT_END', '<script src="' . $this->baseurl . '/components/com_onlinekasse/tmpl/checkout/checkout.js"></script>');
		}
		$this->Event = $this->getModel()->getItem();

		// Nur wenn eine Action gemacht wurde
		if ($this->Event->Action)
		{
			new ManageLastSale($this->Event);
		}
		// Nur wenn ein Event gewählt ist
		elseif ($this->Event->EventId)
		{
			new SaveSales($this->Event);
		}

		// Call the parent display to display the layout file
		parent::display($template);
	}

}