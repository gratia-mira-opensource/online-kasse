<?php

namespace ChristophBerger\Component\OnlineKasse\Site\Helper;

// Kein direkter Aufruf der PHP-Datei
defined('_JEXEC') or die('Restricted Access');

// Hilfsklassen laden
use Joomla\CMS\Factory;
use Joomla\CMS\Language\Text;
use Joomla\CMS\Mail\MailerFactoryInterface;
use PHPMailer\PHPMailer\Exception;

class ManageLastSale
{
	/**
	 * @throws Exception
	 * @since 1.0.0
	 */
	public function __construct($item)
	{

		$AffectedRow = $this->getAffectedRow($item);

		if ($AffectedRow)
		{
			if ($item->Action == 'change_last_payment')
			{
				$this->changePayment($item, $AffectedRow);
			}
			elseif ($item->Action == 'delete_last_payment')
			{
				$this->deletePayment($item, $AffectedRow);
			}
		}
		else
		{
			$config = Factory::getConfig();
			$mail   = Factory::getContainer()->get(MailerFactoryInterface::class)->createMailer();

			$AmountArray = explode(';', $item->PriceCheck);

			$sender = array(
				$config->get('mailfrom'),
				$config->get('fromname')
			);
			$mail->setSender($sender);
			$mail->addRecipient($item->Vendoremail);


			$mail->setSubject('Fail Prozess: ' . $item->Action);
			$body = "Details\n\nEvent: " . $item->EventName . "\nCurrency: " . $item->EventCurrency . "\nTime: " . date('H:i') . "\nBooks: " . $AmountArray[0] . "\nOther: " . $AmountArray[1] . "\nTotal: " . $AmountArray[2] . "\n\nPlease correct this manually.";
			$mail->setBody($body);

			$send = $mail->Send();
			if ($send !== true)
			{
				$item->Message = Text::sprintf('COM_ONLINEKASSE_MSG_ERROR_MANAGE_LAST_SALE_ERROR_MAIL', $AmountArray[0], $AmountArray[1]);
			}
			else
			{
				$item->Message = Text::sprintf('COM_ONLINEKASSE_MSG_ERROR_MANAGE_LAST_SALE', $AmountArray[0], $AmountArray[1]);
			}


		}
	}

	/**
	 * Gibt Informationen zum Eintrag zurück, der verändert werden soll.
	 *
	 * @param $item
	 *
	 * @return object|null
	 * @Link https://stackoverflow.com/questions/78506993/sql-dontt-match-float-with-comma/78507356#78507356
	 * @since 1.0.0
	 */
	private function getAffectedRow($item): object|null
	{

		$AmountArray = explode(';', $item->PriceCheck);
		$book        = round(floatval($AmountArray[0]), 2);
		$other       = round(floatval($AmountArray[1]), 2);
		$total       = round(floatval($AmountArray[2]), 2);

		$db = Factory::getContainer()->get('DatabaseDriver');

		$query = $db->getQuery(true);

		// Die Abfrage formulieren
		$query->select(array('id', 'time', 'amount_total'));
		$query->from('#__ok_entries');
		$query->where($db->quoteName('amount_books') . ' = ' . $db->quote($book) . ' AND ' . $db->quoteName('amount_other') . ' = ' . $db->quote($other) . ' AND ' . $db->quoteName('amount_total') . ' = ' . $db->quote($total));
		$query->order('id DESC');
		$query->setLimit(1);
		// Daten lesen
		$db->setQuery($query);

		return $db->loadObject();


	}

	/**
	 * Löscht die letzte Buchung.
	 *
	 * @param $item
	 * @param $AffectedRow
	 *
	 *
	 * @since 1.0.0
	 */
	private function deletePayment($item, $AffectedRow): void
	{

		$db    = Factory::getContainer()->get('DatabaseDriver');
		$query = $db->getQuery(true);

		$query->delete($db->quoteName('#__ok_entries'));
		$query->where($db->quoteName('id') . '=' . $db->quote($AffectedRow->id));

		$db->setQuery($query);
		$db->execute();

		$item->Message = Text::sprintf('COM_ONLINEKASSE_MSG_SUCCESS_DELETE_LAST_SALE', $AffectedRow->amount_total, $item->EventCurrency, preg_replace('/^[\d-]{10} (\d+:\d+).*$/', '$1', $AffectedRow->time));
	}

	/**
	 * Ändert die Zahlungsart der letzten Buchung.
	 *
	 * @param $item
	 * @param $AffectedRow
	 *
	 *
	 * @since 1.0.0
	 */
	private function changePayment($item, $AffectedRow): void
	{
		$db    = Factory::getContainer()->get('DatabaseDriver');
		$query = $db->getQuery(true);

		// Was soll geändert werden
		$qFelder = [$db->quoteName('payment') . ' = ' . $db->quote($item->Payment)];

		// Wo soll es geändert werden
		$qBedingungen = [$db->quoteName('id') . ' = ' . $db->quote($AffectedRow->id)];

		$query->update($db->quoteName('#__ok_entries'))->set($qFelder)->where($qBedingungen);

		$db->setQuery($query);
		$db->execute();

		$item->Message = Text::sprintf('COM_ONLINEKASSE_MSG_SUCCESS_CHANGE_PAYMENT_LAST_SALE', $AffectedRow->amount_total, $item->EventCurrency, preg_replace('/^[\d-]{10} (\d+:\d+).*$/', '$1', $AffectedRow->time));
	}
}