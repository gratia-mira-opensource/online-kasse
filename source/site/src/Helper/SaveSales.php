<?php

namespace ChristophBerger\Component\OnlineKasse\Site\Helper;

// Kein direkter Aufruf der PHP-Datei
defined('_JEXEC') or die('Restricted Access');

// Hilfsklassen laden
use Exception;
use Joomla\CMS\Factory;
use Joomla\CMS\Language\Text;

class SaveSales
{
	/**
	 * @throws Exception
	 * @since 0.0.5
	 */
	public function __construct($item)
	{
		$this->saveSales($item);
	}

	/**
	 * @throws Exception
	 * @since   0.5.0
	 * @version 1.0.0
	 */
	private function saveSales($item): void
	{

		// Es müssen auch negative Beträge abgebucht werden.
		if (($item->Card or $item->Cash) and $item->Total)
		{

			// $item->Payment
			if ($item->Cash)
			{
				$item->Payment      = 0;
				$AlternativePaymentNumber = 1;
				$AlternativePayment = Text::_('COM_ONLINEKASSE_CARD');
			}
			elseif ($item->Card)
			{
				$item->Payment      = 1;
				$AlternativePaymentNumber = 0;
				$AlternativePayment = Text::_('COM_ONLINEKASSE_CASH');
			}

			if (isset($item->Payment))
			{
				$user   = Factory::getApplication()->getIdentity();
				$UserID = $user->id ?: 0;

				$db = Factory::getContainer()->get('DatabaseDriver');


				$query = $db->getQuery(true);

				$Columns = array('event_id', 'user_id', 'amount_books', 'amount_other', 'amount_total', 'payment', 'note');

				// Insert Values
				$Values = array($db->quote($item->EventId), $db->quote($UserID), $db->quote($item->Books), $db->quote($item->Other), $db->quote($item->Total), $db->quote($item->Payment), $db->quote($item->Note));

				// Die Abfrage formulieren
				$query->insert($db->quoteName('#__ok_entries'));
				$query->columns($db->quoteName($Columns));
				$query->values(implode(',', $Values));

				$db->setQuery($query);
				$db->execute();

				$item->Message     = Text::sprintf('COM_ONLINEKASSE_MSG_SUCCESS_NEW_SALE', $item->Total, $item->EventCurrency, date('H:i'));
				$item->QucikAction =    '<form method="post">
    										<input type="hidden" name="action" value="change_last_payment" />
    										<input type="hidden" name="payment" value="' . $AlternativePaymentNumber . '" />
    									    <input type="hidden" name="price_check" value="' . $item->Books . ';' . $item->Other . ';' . $item->Total . '" />
                            				<button>' . Text::sprintf('COM_ONLINEKASSE_MSG_CHANGE_LAST_PAYMENT', $AlternativePayment) . '</button>
                                    	</form>';
				$item->QucikAction .=   '<form method="post">
    										<input type="hidden" name="action" value="delete_last_payment" />
    										<input type="hidden" name="payment" value="' . $item->Payment . '" />
    									    <input type="hidden" name="price_check" value="' . $item->Books . ';' . $item->Other . ';' . $item->Total . '" />
                            				<button>' . Text::_('COM_ONLINEKASSE_MSG_DELETE_LAST_PAYMENT') . '</button>
                            			</form>';
			}
			else
			{
				$item->Message = Text::_('COM_ONLINEKASSE_MSG_NO_PAYMENT');
			}
		}
		else
		{
			$item->Message = Text::_('COM_ONLINEKASSE_MSG_NO_AMOUNT');
		}
	}
}