<?php
// @todo Alle Sprachschlüssel ändern
// @todo Machen, dass alle Beiträge gefiltert werden können

/**
 * @package     Joomla.Administrator
 * @subpackage  com_onlinekasse
 *
 * @copyright   Copyright (C) 2023 Christoph Berger. All rights reserved.
 * @license     GNU General Public License version 3; see LICENSE
 */

// No direct access to this file
use Joomla\CMS\Language\Text;

defined('_JEXEC') or die('Restricted Access');
?>
<?php echo $this->item->notes; ?>
<table class="table table-striped table-hover">
	<thead>
	<tr>
		<th><?php echo Text::_('COM_ONLINEKASSE_USER'); ?></th>
		<th><?php echo Text::_('COM_ONLINEKASSE_TIME'); ?></th>
		<th><?php echo Text::_('COM_ONLINEKASSE_BOOKS'); ?></th>
		<th><?php echo Text::_('COM_ONLINEKASSE_OTHER'); ?></th>
        <th><?php echo Text::_('COM_ONLINEKASSE_TOTAL'); ?></th>
		<th><?php echo Text::_('COM_ONLINEKASSE_PAYMENT'); ?></th>
		<th><?php echo Text::_('COM_ONLINEKASSE_NOTE'); ?></th>
	</tr>
	</thead>
	<tbody>
	<?php foreach ($this->items as $i => $item) : ?>
		<tr>
			<td><?php echo $item->user_id; ?></td>
			<td><?php echo $item->time; ?></td>
			<td><?php echo $item->amount_books . ' ' . $this->item->currency; ?></td>
			<td><?php echo $item->amount_other . ' ' . $this->item->currency; ?></td>
			<td><?php echo $item->amount_total . ' ' . $this->item->currency; ?></td>
			<td><?php echo $item->payment ? Text::_('COM_ONLINEKASSE_CARD') : Text::_('COM_ONLINEKASSE_CASH'); ?></td>
			<td><?php echo $item->note; ?></td>
		</tr>
	<?php endforeach; ?>
	</tbody>
</table>