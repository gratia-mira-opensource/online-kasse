<?php

use Joomla\CMS\Factory;
use Joomla\CMS\HTML\HTMLHelper;
use Joomla\CMS\Router\Route;

$app = Factory::getApplication();
$input = $app->input;

$wa = $this->document->getWebAssetManager();

$wa->useScript('keepalive');
$wa->useScript('form.validate');

$layout  = 'edit';
$tmpl = $input->get('tmpl', '', 'cmd') === 'component' ? '&tmpl=component' : '';
?>
<form action="<?php echo Route::_('index.php?option=com_onlinekasse&layout=' . $layout . $tmpl . '&id=' . (int) $this->item->id); ?>" method="post" name="adminForm" id="event-form" class="form-validate">

	<?php echo $this->form->renderField('name'); ?>
	<?php echo $this->form->renderField('date'); ?>
	<?php echo $this->form->renderField('currency'); ?>
	<?php echo $this->form->renderField('location'); ?>
	<?php echo $this->form->renderField('notes'); ?>

<!--	<input type="hidden" name="task" value="onlinekasse.edit" />-->
	<input type="hidden" name="task" value="" />
	<?php echo HTMLHelper::_('form.token'); ?>
</form>