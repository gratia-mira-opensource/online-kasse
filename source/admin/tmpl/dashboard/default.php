<?php
// @todo Alle Sprachschlüssel ändern
// @todo Machen, dass alle Beiträge gefiltert werden können

/**
 * @package     Joomla.Administrator
 * @subpackage  com_onlinekasse
 *
 * @copyright   Copyright (C) 2023 Christoph Berger. All rights reserved.
 * @license     GNU General Public License version 3; see LICENSE
 */

// No direct access to this file
use Joomla\CMS\Language\Text;

defined('_JEXEC') or die('Restricted Access');
?>

<table class="table table-striped table-hover">
    <thead>
        <tr>
            <th><?php echo Text::_('COM_ONLINEKASSE_NAME'); ?></th>
            <th><?php echo Text::_('COM_ONLINEKASSE_LOCATION'); ?></th>
            <th><?php echo Text::_('COM_ONLINEKASSE_DATE'); ?></th>
            <th><?php echo Text::_('COM_ONLINEKASSE_BOOKS'); ?></th>
            <th><?php echo Text::_('COM_ONLINEKASSE_OTHER'); ?></th>
            <th><?php echo Text::_('COM_ONLINEKASSE_CARD'); ?></th>
            <th><?php echo Text::_('COM_ONLINEKASSE_CASH'); ?></th>
            <th><?php echo Text::_('COM_ONLINEKASSE_TOTAL'); ?></th>
            <th><?php echo Text::_('COM_ONLINEKASSE_SALES'); ?></th>
        </tr>
    </thead>
    <tbody>
        <?php foreach ($this->items as $i => $item) : ?>
            <tr>
                <td>
                    <?php echo GMF_Layout::erstelleLink('?option=com_onlinekasse&view=event&layout=edit&id=' . $item->id,'✎',Text::_('COM_ONLINEKASSE_EVENT_EDIT')); ?>
                    <?php echo GMF_Layout::erstelleLink('?option=com_onlinekasse&view=detail&id=' . $item->id,$item->name,Text::_('COM_ONLINEKASSE_EVENT_DETAIL')); ?>
                </td>
                <td><?php echo $item->location; ?></td>
                <td><?php echo $item->date; ?></td>
                <td><?php echo $item->amount_books . ' ' . $item->currency; ?></td>
                <td><?php echo $item->amount_other . ' ' . $item->currency; ?></td>
                <td><?php echo $item->amount_card . ' ' . $item->currency; ?></td>
                <td><?php echo $item->amount_cash . ' ' . $item->currency; ?></td>
                <td><?php echo $item->amount_total . ' ' . $item->currency; ?></td>
                <td><?php echo $item->sales; ?></td>
            </tr>
        <?php endforeach; ?>
    </tbody>
</table>