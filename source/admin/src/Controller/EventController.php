<?php
namespace ChristophBerger\Component\OnlineKasse\Administrator\Controller;

\defined('_JEXEC') or die;

use Joomla\CMS\MVC\Controller\FormController;

class EventController extends FormController
{
	protected $view_list = 'dashboard';
}