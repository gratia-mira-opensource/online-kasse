<?php

namespace ChristophBerger\Component\OnlineKasse\Administrator\Controller;

defined('_JEXEC') or die;

use Joomla\CMS\MVC\Controller\BaseController;

/**
 * @package     Joomla.Administrator
 * @subpackage  com_onlinekasse
 *
 * @copyright   Copyright (C) 2023 Christoph Berger All rights reserved.
 * @license     GNU General Public License version 3; see LICENSE
 */

/**
 * Default Controller of OnlineKasse component
 *
 * @package     Joomla.Administrator
 * @subpackage  com_onlinekasse
 */
class DisplayController extends BaseController {
    /**
     * The default view for the display method.
     *
     * @var string
     */
    protected $default_view = 'Dashboard';

    public function display($cachable = false, $urlparams = array()) {

        return parent::display($cachable, $urlparams);
    }

}