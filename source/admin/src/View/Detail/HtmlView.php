<?php

namespace ChristophBerger\Component\OnlineKasse\Administrator\View\Detail;

defined('_JEXEC') or die;

use Joomla\CMS\Factory;
use Joomla\CMS\MVC\View\HtmlView as BaseHtmlView;
use Joomla\CMS\Toolbar\ToolbarHelper;

/**
 * @package     Joomla.Administrator
 * @subpackage  com_onlinekasse
 *
 * @copyright   Copyright (C) 2023 Christoph Berger. All rights reserved.
 * @license     GNU General Public License version 3; see LICENSE
 */

/**
 * "Detail" Admin View
 */
class HtmlView extends BaseHtmlView
{

	/**
	 * Display the main "Online Kasse" view
	 *
	 * @param   string  $tpl  The name of the template file to parse; automatically searches through the template paths.
	 *
	 * @return  void
	 * @since 0.0.5
	 */
	function display($tpl = null)
	{

		// $this->items = $this->get('Items');
		// Im Moment scheit es das "DashbordModel" nicht zu erkennen.
		// Alle Versuche blieben erfolglos.
		// Für den Augenblick reicht auch eine einfache Abfrage.
		// @todo Workaround aufheben sobald als möglich

		$db = Factory::getContainer()->get('DatabaseDriver');

		// Detaileinträge zum Event
		$query = $db->getQuery(true);
		$query->select('*');
		$query->from('#__ok_entries');
		$query->where('event_id = ' . $_GET['id']);
		$query->order('time DESC');
		$db->setQuery($query);
		$this->items = $db->loadObjectList();

		$db = Factory::getContainer()->get('DatabaseDriver');

		// Informationen zum Event
		$query = $db->getQuery(true);
		$query->select('*');
		$query->from('#__ok_events');
		$query->where('id = ' . $_GET['id']);
		$db->setQuery($query);
		$this->item = $db->loadObject();

		ToolBarHelper::title($this->item->date . ': ' . $this->item->name . ' (' . $this->item->location . ')');
		ToolbarHelper::back();

		parent::display($tpl);
	}
}
