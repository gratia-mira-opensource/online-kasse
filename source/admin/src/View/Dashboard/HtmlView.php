<?php

namespace ChristophBerger\Component\OnlineKasse\Administrator\View\Dashboard;

defined('_JEXEC') or die;

use Joomla\CMS\Factory;
use Joomla\CMS\Language\Text;
use Joomla\CMS\MVC\View\HtmlView as BaseHtmlView;
use Joomla\CMS\Toolbar\ToolbarHelper;

/**
 * @package     Joomla.Administrator
 * @subpackage  com_onlinekasse
 *
 * @copyright   Copyright (C) 2023 Christoph Berger. All rights reserved.
 * @license     GNU General Public License version 3; see LICENSE
 */

/**
 * Main "Online Kalender" Admin View
 */
class HtmlView extends BaseHtmlView
{

	/**
	 * Display the main "Online Kasse" view
	 *
	 * @param   string  $tpl  The name of the template file to parse; automatically searches through the template paths.
	 *
	 * @return  void
	 * @since 0.0.5
	 */
	function display($tpl = null)
	{

		// $this->items = $this->get('Items');
		// Im Moment scheit es das "DashbordModel" nicht zu erkennen.
		// Alle Versuche blieben erfolglos.
		// Für den Augenblick reicht auch eine einfache Abfrage.
		// @todo Workaround aufheben sobald als möglich

		$db = Factory::getContainer()->get('DatabaseDriver');

		$query = $db->getQuery(true);
		$query->select('*');
		$query->from('#__ok_events');
		$query->order('date DESC');
		$db->setQuery($query);

		$items = $db->loadObjectList();

		// Load Data from the different detail
		foreach ($items as $item) {
			$query = $db->getQuery(true);
			$query->select('amount_books, amount_other, amount_total, payment');
			$query->from('#__ok_entries');
			$query->where('event_id = ' . $item->id);
			$db->setQuery($query);
			$details = $db->loadObjectList();

			$amount_books = 0;
			$amount_other = 0;
			$card = 0;
			$cash = 0;
			$total = 0;
			foreach ($details as $detail){
				$amount_books += $detail->amount_books;
				$amount_other += $detail->amount_other;
				if($detail->payment == 1) {
					$card += $detail->amount_total;
				} else {
					$cash += $detail->amount_total;
				}
				$total += $detail->amount_total;
			}

			$item->sales = count($details);
			$item->amount_books = $amount_books;
			$item->amount_other = $amount_other;
			$item->amount_card = $card;
			$item->amount_cash = $cash;
			$item->amount_total = $total;
		}
		$this->items = $items;


		ToolBarHelper::title(Text::_('COM_ONLINEKASSE_DASHBOARD') . ' – ' . date('d.m.Y'));
		ToolBarHelper::preferences('com_onlinekasse');

		parent::display($tpl);
	}
}
