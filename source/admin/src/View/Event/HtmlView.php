<?php

namespace ChristophBerger\Component\OnlineKasse\Administrator\View\Event;

defined('_JEXEC') or die;

use Joomla\CMS\Factory;
use Joomla\CMS\Language\Text;
use Joomla\CMS\MVC\View\HtmlView as BaseHtmlView;
use Joomla\CMS\Toolbar\ToolbarHelper;
use Joomla\CMS\Toolbar\Toolbar;

/**
 * @package     Joomla.Administrator
 * @subpackage  com_onlinekasse
 *
 * @copyright   Copyright (C) 2023 Christoph Berger. All rights reserved.
 * @license     GNU General Public License version 3; see LICENSE
 */

/**
 * Event "Online Kalender" Admin View
 */
class HtmlView extends BaseHtmlView
{

	/**
	 * Display the event "Online Kasse" view
	 *
	 * @param   string  $tpl  The name of the template file to parse; automatically searches through the template paths.
	 *
	 * @return  void
	 * @since 0.0.5
	 */
	function display($tpl = null)
	{
		$this->form = $this->get('Form');
		$this->item = $this->get('Item');

		$this->addToolbar();

		parent::display($tpl);
	}


	protected function addToolbar()
	{
		Factory::getApplication()->getInput()->set('hidemainmenu', true);
		ToolbarHelper::title(Text::_('COM_ONLINEKASSE_ADD_NEW_EVENT'));

		ToolbarHelper::apply('event.apply');
		ToolbarHelper::save2new('event.save2new');
		ToolbarHelper::save('event.save');
		ToolbarHelper::cancel('event.cancel');
	}
}
