<?php

namespace ChristophBerger\Component\OnlineKasse\Administrator\Model;

defined('_JEXEC') or die;

use Joomla\CMS\Factory;
use Joomla\CMS\MVC\Model\AdminModel;

class EventModel extends AdminModel
{
	public $typeAlias = 'com_onlinekasse.event';
	public function getForm($data = array(), $loadData = true)
	{
		$form = $this->loadForm($this->typeAlias, 'event', array('control' => 'jform', 'load_data' => $loadData));

		if (empty($form))
		{
			return false;
		}

	return $form;
	}
	protected function loadFormData()
	{
		$app  = Factory::getApplication();
		$data = $app->getUserState('com_onlinekasse.edit.event.data', []);

		if (empty($data))
		{
			$data = $this->getItem();
		}

		$this->preprocessData($this->typeAlias, $data);

		return $data;
	}

	protected function prepareTable($table)
	{
		$table->generateAlias();
	}
}