<?php

namespace ChristophBerger\Component\OnlineKasse\Administrator\Model;

\defined('_JEXEC') or die;

use Joomla\CMS\MVC\Model\ListModel;

class DashbordModel extends ListModel
{

	public function __construct($config = [])
	{
		parent::__construct($config);
	}

	protected function getListQuery()
	{
		// Create a new query object.
		$db= $this->getDatabase();
		$query = $db->getQuery(true);

		// Select the required fields from the table.
		$query->select(
			$db->quoteName(['id', 'name'])
		);
		$query->from($db->quoteName('#__ok_events'));

		return $query;
	}
}