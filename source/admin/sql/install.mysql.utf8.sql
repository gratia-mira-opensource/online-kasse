CREATE TABLE `#__ok_entries`
(
    `id`           INT            NOT NULL AUTO_INCREMENT,
    `event_id`     INT            NULL     DEFAULT NULL,
    `user_id`      INT            NULL     DEFAULT NULL,
    `time`         TIMESTAMP      NOT NULL DEFAULT CURRENT_TIMESTAMP,
    `amount_books` DECIMAL(20, 2) NOT NULL DEFAULT '0',
    `amount_other` DECIMAL(20, 2) NOT NULL DEFAULT '0',
    `amount_total` DECIMAL(20, 2) NULL     DEFAULT NULL,
    `payment`      INT(2)         NULL     DEFAULT NULL,
    `note`         TEXT           NULL     DEFAULT NULL,
    PRIMARY KEY (`id`)
) ENGINE = InnoDB;

CREATE TABLE `#__ok_events`
(
    `id`       INT     NOT NULL AUTO_INCREMENT,
    `name`     TEXT    NULL DEFAULT NULL,
    `date`     DATE    NULL DEFAULT NULL,
    `currency` CHAR(3) NULL DEFAULT NULL,
    `location` TEXT    NULL DEFAULT NULL,
    `notes`    TEXT    NULL DEFAULT NULL,
    PRIMARY KEY (`id`)
) ENGINE = InnoDB;