ALTER TABLE `#__ok_entries` CHANGE `amount_books` `amount_books` DECIMAL(20,2) NOT NULL DEFAULT '0.00';
ALTER TABLE `#__ok_entries` CHANGE `amount_other` `amount_other` DECIMAL(20,2) NOT NULL DEFAULT '0.00';
ALTER TABLE `#__ok_entries` CHANGE `amount_total` `amount_total` DECIMAL(20,2) NULL DEFAULT NULL;