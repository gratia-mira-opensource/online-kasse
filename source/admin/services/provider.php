<?php

defined('_JEXEC') or die;

use Joomla\CMS\Dispatcher\ComponentDispatcherFactoryInterface;
use Joomla\CMS\Extension\ComponentInterface;
use Joomla\CMS\Extension\MVCComponent;
use Joomla\CMS\Extension\Service\Provider\ComponentDispatcherFactory;
use Joomla\CMS\Extension\Service\Provider\MVCFactory;
use Joomla\CMS\MVC\Factory\MVCFactoryInterface;
use Joomla\DI\Container;
use Joomla\DI\ServiceProviderInterface;

return new class implements ServiceProviderInterface {

    public function register(Container $container): void {

		// Framework testen
	    if(!defined('GM_FRAMEWORK') or version_compare(constant('GM_FRAMEWORK'), '0.0.8') < 0) {
			if(defined('GM_FRAMEWORK')) {
				$Aufgabe = 'aktualisieren';
			} else {
				$Aufgabe = 'installieren oder <a href="https://docs.joomla.org/Help4.x:Plugins:_Name_of_Plugin/de" title="zur Joomla-Hilfeseite" target="_blank">aktivieren</a>';
			}

			exit('Bitte <a href="https://gitlab.com/gratia-mira-opensource/gratia-mira-framework/-/releases" title="zum Framework" target="_blank">GMFramework </a>' .  $Aufgabe . '!');
		}

	    $container->registerServiceProvider(new MVCFactory('\\ChristophBerger\\Component\\OnlineKasse'));
        $container->registerServiceProvider(new ComponentDispatcherFactory('\\ChristophBerger\\Component\\OnlineKasse'));
        $container->set(
            ComponentInterface::class,
            function (Container $container) {
                $component = new MVCComponent($container->get(ComponentDispatcherFactoryInterface::class));
                $component->setMVCFactory($container->get(MVCFactoryInterface::class));

                return $component;
            }
        );
    }
};